FROM multiarch/debian-debootstrap:amd64-buster
#FROM multiarch/debian-debootstrap:armel-buster
#FROM multiarch/debian-debootstrap:armhf-buster
#FROM multiarch/debian-debootstrap:arm64-buster

ENV PYTHON_CONFIGURE_OPTS "--enable-shared"

ENV VERSION_3_6 3.6.9
ENV VERSION_3_7 3.7.8

RUN apt-get -qy update && \
    DEBIAN_FRONTEND=noninteractive apt-get -yq --no-install-recommends --ignore-hold -m install \
        build-essential \
        curl \
        git \
        sudo \
        wget \
        make \
        build-essential \
        libssl-dev \
        zlib1g-dev \
        libbz2-dev \
        libffi-devel \
        libreadline-dev \
        libsqlite3-dev \
        wget \
        curl \
        llvm \
        libncurses5-dev \
        libncursesw5-dev \
        xz-utils \
        tk-dev \
        && rm -rf /var/lib/apt/lists/*


###############################################################################
##  Python
###############################################################################

RUN cd /tmp/ && wget --no-check-certificate https://www.python.org/ftp/python/${VERSION_3_6}/Python-${VERSION_3_6}.tgz && tar xaf Python-${VERSION_3_6}.tgz

RUN cd /tmp/Python-${VERSION_3_6} && ./configure --with-ensurepip=install --enable-optimizations --enable-shared && make && make altinstall

RUN cd /tmp/ && wget --no-check-certificate https://www.python.org/ftp/python/${VERSION_3_7}/Python-${VERSION_3_7}.tgz && tar xaf Python-${VERSION_3_7}.tgz

RUN cd /tmp/Python-${VERSION_3_7} && ./configure --with-ensurepip=install --enable-optimizations --enable-shared && make && make altinstall
